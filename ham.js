
function show() {
  document.getElementById('read').style.cssText = 'height:500px;overflow:hidden;transition:height 500ms ease;';
  document.getElementById('show').style.cssText = 'display:none;';
}

function hide() {
  document.getElementById('read').style.cssText = 'height:0px;overflow:hidden;transition:height 500ms ease;';
  document.getElementById('show').style.cssText = 'display:block;';
}
